/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.cosw.examples.productorders;

import edu.eci.cosw.examples.productorders.repositories.ClientsRepository;
import edu.eci.cosw.samples.model.Cliente;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author amoto
 */
@RestController
@RequestMapping(path = "/clients")
public class ClientsController {
    @Autowired
    ClientsRepository cr;
    
    @RequestMapping(method = RequestMethod.GET,value = "/productPrice/{price}")
    @ResponseBody
    public ResponseEntity<List<Cliente>> getAllProds(@PathVariable int price) {
        try {
            return ResponseEntity.ok().body(cr.getClientesByPrice(price));
        } catch (Exception ex) {
            Logger.getLogger(ProductsController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
