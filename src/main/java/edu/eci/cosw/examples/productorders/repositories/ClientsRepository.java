/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.cosw.examples.productorders.repositories;

import edu.eci.cosw.samples.model.Cliente;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author amoto
 */
public interface ClientsRepository extends JpaRepository<Cliente, Integer> {
    @Query("select c from Pedido p join p.detallesPedidos dp join p.cliente c join dp.producto prod where prod.precio > :price group by idcliente")
    public List<Cliente> getClientesByPrice(@Param("price") long price);
}
