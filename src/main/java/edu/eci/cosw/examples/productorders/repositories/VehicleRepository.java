/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.cosw.examples.productorders.repositories;

import edu.eci.cosw.samples.model.Vehiculo;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author amoto
 */
public interface VehicleRepository extends JpaRepository<Vehiculo, String> {
    @Query("select d.vehiculo from Despacho d inner join d.pedidos ped inner join ped.detallesPedidos dp inner join dp.producto p where p.idproducto=:n group by placa")
    public List<Vehiculo> getVehicleByProductId(@Param("n") int n);
}
